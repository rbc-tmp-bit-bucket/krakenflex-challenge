## Installation

```
cd outage_enhancer
python3 -m venv venv
source venv/bin/activate
python3 -m pip install -r requirements.txt
```

## Usage

```
export KRAKENFLEX_API_KEY=XXX_YOUR_KEY
./update_outages.py --site_id norwich-pear-tree --not_before 2022-01-01T00:00:00.000+00:00
```

## Unit Tests

`outage_enhancer` has a small suite of tests

```
python -m unittest
```
## Technical Approach

I opted to auto generate an api client library hosted in its own public repo[https://bitbucket.org/rbc-tmp-bit-bucket/kraken-api-client/]. This ended up consuming most of my resources, and means there isn't actually much code here to look at.

There is a simple shell script, `generate_api_client.sh` to regenerate this, though you must install openapi-generator before you do this

### TODOS

* Better tooling around code generation
* Error handling. At present failed API calls will just bubble up as an exception. It wasn't clear to me if adding retry/backoff logic was actually desirable. If the application was running in particular environments, e.g. a lambda function, you might actually want it to hard fail and for the whole thing to be retried. You may also not want to introduce a significant gap between the queries
* setup.py, perhaps also Dockerfile
