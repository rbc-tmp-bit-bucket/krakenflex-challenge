import unittest
from datetime import datetime

from kraken_api_client.model.enhanced_outages import EnhancedOutages
from kraken_api_client.model.outages import Outages
from kraken_api_client.model.site_info import SiteInfo
from kraken_api_client.model.site_info_devices import SiteInfoDevices
from outage_enhancer import outage_enhancer


def example_site() -> SiteInfo:
    devices = [
        SiteInfoDevices("002b28fc-283c-47ec-9af2-ea287336dc1b", "Battery 1"),
    ]
    return SiteInfo("test-site", "Test Site", devices)


class TestEnhancement(unittest.TestCase):
    def setUp(self):
        outages = Outages(
            [
                {
                    "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
                    "begin": datetime.fromisoformat("2022-04-08T00:00:00.000+00:00"),
                    "end": datetime.fromisoformat("2022-04-09T00:00:00.000+00:00"),
                }
            ]
        )
        not_before = datetime.fromisoformat("2022-01-01T00:00:00.000+00:00")
        self.result = outage_enhancer(outages, example_site(), not_before)

    def test_not_filtered_out(self):
        """
        A single correct outage is not filtered out
        """
        self.assertEqual(len(self.result.value), 1)

    def test_field_values(self):
        """
        The enhanced outage is populated with the correct values
        """
        enhanced_outage = self.result.value[0]
        self.assertEqual(enhanced_outage["id"], "002b28fc-283c-47ec-9af2-ea287336dc1b")
        self.assertEqual(enhanced_outage["name"], "Battery 1")
        self.assertEqual(
            enhanced_outage["begin"],
            datetime.fromisoformat("2022-04-08T00:00:00.000+00:00"),
        )
        self.assertEqual(
            enhanced_outage["end"],
            datetime.fromisoformat("2022-04-09T00:00:00.000+00:00"),
        )


class TestFiltersAbsentDevice(unittest.TestCase):
    def setUp(self):
        outages = Outages(
            [
                {
                    "id": "bb2d2ab2-b8d4-11ec-9c6e-576c25be6b45",
                    "begin": datetime.fromisoformat("2022-04-08T00:00:00.000+00:00"),
                    "end": datetime.fromisoformat("2022-04-09T00:00:00.000+00:00"),
                }
            ]
        )
        not_before = datetime.fromisoformat("2022-01-01T00:00:00.000+00:00")
        self.result = outage_enhancer(outages, example_site(), not_before)

    def test_no_results(self):
        """
        No outages in the list match the devices on the site
        Consequently this list should be empty
        """
        self.assertEqual(len(self.result.value), 0)


class TestFiltersOnDate(unittest.TestCase):
    def setUp(self):
        outages = Outages(
            [
                {
                    "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
                    "begin": datetime.fromisoformat("2022-04-08T00:00:00.000+00:00"),
                    "end": datetime.fromisoformat("2022-04-09T00:00:00.000+00:00"),
                }
            ]
        )
        not_before = datetime.fromisoformat("2022-04-08T00:00:01.000+00:00")
        self.result = outage_enhancer(outages, example_site(), not_before)

    def test_no_results(self):
        """
        We're filtering results from before the cut off so there should be no entries
        """
        self.assertEqual(len(self.result.value), 0)
