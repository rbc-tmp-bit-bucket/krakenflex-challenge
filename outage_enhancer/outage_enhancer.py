from datetime import datetime

from kraken_api_client.model.enhanced_outages import EnhancedOutages
from kraken_api_client.model.outages import Outages
from kraken_api_client.model.site_info import SiteInfo


def outage_enhancer(
    outages: Outages, site: SiteInfo, not_before: datetime
) -> EnhancedOutages:
    # build a look of device to device name
    device_lookup = dict()
    for device in site.devices:
        device_lookup[device["id"]] = device["name"]

    # loop through list of outages
    # enhance those which belong to our site
    enhanced_outages = list()
    for outage in outages.value:
        if outage["id"] in device_lookup and outage["begin"] >= not_before:

            enhanced_outages.append({**outage, **{"name": device_lookup[outage["id"]]}})
    # return outages using EnhancedOutages list
    return EnhancedOutages(enhanced_outages)
