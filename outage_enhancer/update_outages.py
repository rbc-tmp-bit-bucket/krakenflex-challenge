#!/usr/bin/env python3

import argparse
import os
from datetime import datetime

import kraken_api_client
from kraken_api_client.api import exercise_api

from outage_enhancer import outage_enhancer


def perform_update_for_site_id(site_id: str, not_before: datetime):
    """
    Do the http requests to retrieve and combine outage info for a single site
    """
    conf = kraken_api_client.Configuration(
        host="https://api.krakenflex.systems/interview-tests-mock-api/v1"
    )
    conf.api_key["api_key"] = os.environ["KRAKENFLEX_API_KEY"]
    with kraken_api_client.ApiClient(conf) as api_client:
        api_instance = exercise_api.ExerciseApi(api_client)
        outage_response = api_instance.outages_get()
        site_response = api_instance.site_info_site_id_get(site_id)
        # enhance the outages
        ehos = outage_enhancer(outage_response, site_response, not_before)
        # print(ehos)
        # post back to the api
        api_instance.site_outages_site_id_post(site_id, enhanced_outages=ehos)


def main():
    parser = argparse.ArgumentParser(description="Tool for updating site outages")
    parser.add_argument("--site_id", type=str, required=True)
    parser.add_argument(
        "--not_before",
        type=str,
        required=True,
        help="ISO timestamp, we shouldn't include outages before this",
    )
    args = parser.parse_args()
    not_before = datetime.fromisoformat(args.not_before)
    perform_update_for_site_id(args.site_id, not_before)


if __name__ == "__main__":
    main()
